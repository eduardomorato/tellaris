using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UserInterfaceManager : MonoBehaviour
{
    [SerializeField] private UserInterfaceDataScriptableObject _uiData;
    [SerializeField] private TMP_Text _healthText;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Image _healthBarSprite;
    
    private float _healthBarFilling = 0;
    private float _healthBarSpeed = 0.5f;

    private void Start() {
        _restartButton.onClick.AddListener(() => RestartLevel()); 
    }

    private void RestartLevel(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
    }

    private void Update(){
        float speed = _healthBarSpeed;
        if (_healthBarFilling - _uiData.GetHealthPercentage() > 0.1 || _healthBarFilling - _uiData.GetHealthPercentage() < -0.1) speed = speed * 10;
        _healthBarFilling = Mathf.MoveTowards(_healthBarFilling, (_uiData.GetHealthPercentage()), speed * Time.deltaTime);
        _healthBarSprite.fillAmount = _healthBarFilling;
        
        _healthText.text = _uiData.GetPlayerHealth().ToString();
    }
}
