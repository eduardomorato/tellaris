using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private float _followSpeed, yPosition;
    [SerializeField] private Transform _player;

    private void FixedUpdate() {
        if (!_player) return;
        Vector2 targetPos = _player.position;
        Vector2 smoothPos = Vector2.Lerp(transform.position, targetPos, _followSpeed * Time.fixedDeltaTime);
        transform.position = new Vector3(smoothPos.x, smoothPos.y + yPosition, -10);
    }
}
