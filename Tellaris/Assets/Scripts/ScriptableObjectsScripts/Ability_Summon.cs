using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[CreateAssetMenu(fileName = "Ability_SummonScriptableObject", menuName = "ScriptableObjects/Ability/Summon")]
public class Ability_Summon : AbilityScriptableObject
{
    [SerializeField] private GameObject _minion;
    [SerializeField] private AudioClip _attackSound;
    [SerializeField] private int _minionNumber;
    [SerializeField] private int _damage;

    // Spawna o devido número de minions e 
    public override void Activate(GameObject parent, Transform target) {
        //todo play attack sound
        for (int i = 0; i < _minionNumber; i++){
            GameObject spwan = Instantiate(_minion, parent.transform.localPosition, Quaternion.identity) as GameObject;
            spwan.transform.GetComponent<Enemy>().SetActualTarget(target);
            spwan.transform.position = spwan.transform.position * (new Vector2(Random.Range(1f, 0.98f), Random.Range(0.8f, 1.2f)));
            spwan.GetComponent<Enemy>().SetActualTarget(target);
        }
    }
}
