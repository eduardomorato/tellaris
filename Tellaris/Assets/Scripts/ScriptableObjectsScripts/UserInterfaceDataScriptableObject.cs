using UnityEngine;

[CreateAssetMenu(fileName = "UserInterfaceDataScriptableObject", menuName = "ScriptableObjects/User Interface Data")]
public class UserInterfaceDataScriptableObject : ScriptableObject
{
    private float _health;
    private float _maxHealth;
    private bool _isAlive;
    
    public void SetPlayerHealth(float health){
        _health = health;
    }

    public void SetPlayerAlive(bool alive){
        _isAlive = alive;
    }

    public void SetPlayerMaxHealth(float max){
        _maxHealth = max;
    }

    public float GetPlayerHealth(){
        return _health;
    }

    public bool GetPlayerAlive(){
        return _isAlive;
    }

    public float GetPlayerMaxHealth(){
        return _maxHealth;
    }

    public float GetHealthPercentage(){
        return _health / _maxHealth;
    }
}