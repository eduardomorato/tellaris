using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using EZCameraShake;

[CreateAssetMenu(fileName = "Ability_ExplodeScriptableObject", menuName = "ScriptableObjects/Ability/Explode")]
public class Ability_Explode : AbilityScriptableObject
{
    [SerializeField] private AudioClip _attackSound;
    [SerializeField] private int _damage;
    [SerializeField] private float _range;
    [SerializeField] private GameObject _destroyEffect;

    public override void Activate(GameObject sender, Transform target){
        //todo play attack sound
        foreach (Collider2D collider in Physics2D.OverlapCircleAll(sender.transform.position, _range)){
            if (collider == sender.GetComponent<Collider2D>()) continue;
            GameObject des = Instantiate(_destroyEffect, sender.transform.position, Quaternion.identity) as GameObject;
            des.transform.localScale = des.transform.localScale * 2f;
            if (collider.tag == "Player") {
                collider.GetComponent<Player>().TakeDamage(sender.transform, 10);
            }
        }
        CameraShaker.Instance.ShakeOnce(1f, 1f, 0.1f, 0.6f);
        Destroy(sender);
    }
}
