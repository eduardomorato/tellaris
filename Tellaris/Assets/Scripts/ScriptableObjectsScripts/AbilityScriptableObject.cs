using UnityEngine;

public class AbilityScriptableObject : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private float _cooldownTime;
    [SerializeField] private float _activeTime;

    public virtual void Activate(GameObject parent, Transform target){ }

    public string GetName(){
        return _name;
    }

    public float GetCooldownTime(){
        return _cooldownTime;
    }

    public float GetActiveTime(){
        return _activeTime;
    }
}
