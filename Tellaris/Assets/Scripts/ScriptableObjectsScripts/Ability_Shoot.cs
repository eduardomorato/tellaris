using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using EZCameraShake;

[CreateAssetMenu(fileName = "Ability_ShootScriptableObject", menuName = "ScriptableObjects/Ability/Shoot")]
public class Ability_Shoot : AbilityScriptableObject
{
    [SerializeField] private AudioClip _attackSound;
    [SerializeField] private int _damage;
    [SerializeField] private float _bulletSize;
    [SerializeField] private float _bulletSpeed;
    [SerializeField] private float _knockbackForce;
    [SerializeField] private GameObject _bullet;

    public override void Activate(GameObject sender, Transform posReference){
        //todo play attack sound
        CameraShaker.Instance.ShakeOnce(0.5f, 0.5f, 0.1f, 0.3f);
        GameObject newBullet = Instantiate(_bullet, posReference.position, posReference.rotation) as GameObject;
        Vector2 direction  = ((Vector2)sender.GetComponent<PlayerController>().GetPointerInput() - (Vector2)sender.transform.position).normalized;
        newBullet.transform.localScale = Vector3.one * _bulletSize;
        newBullet.GetComponent<Bullet>().PrepareBullet(_damage, sender);
        newBullet.GetComponent<Rigidbody2D>().velocity = _bulletSpeed * direction;
        if (sender.GetComponent<Player>()) sender.GetComponent<Player>().Knockback(posReference, _knockbackForce);
    }
}
