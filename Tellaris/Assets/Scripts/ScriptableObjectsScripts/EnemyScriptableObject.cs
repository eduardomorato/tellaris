using UnityEngine;

[CreateAssetMenu(fileName = "EnemyScriptableObject", menuName = "ScriptableObjects/Enemy")]
public class EnemyScriptableObject : ScriptableObject
{
    //todo Encapsular isto
    [Header("Player maximum Health Points")]
    public float maxHealth;
    [Header("How far the enemy sees the player")]
    public float searchRadius;
    [Header("Max speed for each state")]
    public float wanderingSpeed;
    public float chaseStateSpeed;
    public float fleeStateSpeed;
    [Header("Enemy stays still for some time between this range when wandering")]
    public float maxWanderingTime;
    public float minWanderingTime;
    [Header("Enemy chase for this time before attacking")]
    public float chaseStateDuration;
    [Header("Enemy keeps attacking for this time")]
    public float attackStateDuration;
    [Header("Distance the enemy flees")]
    public float fleeDistance;

    public AbilityScriptableObject enemyAbility;
    public LayerMask RaycastLayer;

    public Transform TargetInSight(GameObject sender, string targetTag, float extraRadius = 0){
        
        foreach (Collider2D collider in Physics2D.OverlapCircleAll(sender.transform.position, searchRadius + extraRadius)){
            if (collider.tag == sender.tag) continue;
            if (collider.tag == targetTag) {
                RaycastHit2D hit = Physics2D.Raycast(sender.transform.position, (collider.transform.position - sender.transform.position), (searchRadius + extraRadius) * 2, RaycastLayer);
                if(hit.transform != null && hit.transform.tag == targetTag) return hit.transform;
            }
        }
        return null;
    }
}
