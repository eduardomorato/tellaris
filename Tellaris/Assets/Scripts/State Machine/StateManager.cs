using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    [SerializeField] private State currentState;
    private Enemy _enemy;

    private void Awake() {
        _enemy = transform.GetComponent<Enemy>();
    }

    private void FixedUpdate() {
        RunStateMachine();
    }

    private void RunStateMachine(){
        State nextState = currentState?.RunCurrentState();
        if (nextState != null) SwithToNextState(nextState);
    }

    private void SwithToNextState(State nextState){
        currentState = nextState;
    }
}
