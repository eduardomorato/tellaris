using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : State
{
    [SerializeField] private AttackState _attackState;
    [SerializeField] private bool _isInAttackRange;

    /*
        private informacões necessarias 

        Código pra atacar () {
            ataque bla bla bla
        }
    */

    public override State RunCurrentState(){
        if (_isInAttackRange) return _attackState;
        else return this;
    }
}
