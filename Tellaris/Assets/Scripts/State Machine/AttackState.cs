using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : State
{
    public override State RunCurrentState()
    {
        return this;
    }

// Como eu uso essa parada?
// 1 - Usar o código do state apenas para códigos que mudam o state
// 2 - Usar para códigos que mudam o state e para códigos que devem ser rodados neste state
// 3 - Usar apenas para armazenar informações sobre o state 

}
