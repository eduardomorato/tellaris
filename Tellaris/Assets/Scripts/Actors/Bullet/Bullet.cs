using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private GameObject _destroyEffect;
    private GameObject _father;
    private float _damage;

    public void PrepareBullet(float damage, GameObject father){
        _damage = damage;
        _father = father;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.transform.tag == "Enemy"){
            other.transform.GetComponent<Enemy>().TakeDamage(_father.transform, _damage);
        }
        Instantiate(_destroyEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
