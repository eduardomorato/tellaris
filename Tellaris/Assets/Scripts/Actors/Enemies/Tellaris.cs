using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random=UnityEngine.Random;
using Pathfinding;
using System;

// Funções específicas e states do Tellaris

public class Tellaris : MonoBehaviour
{
    // Sprite, inimigo e abilidades do inimigo
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private EnemyScriptableObject _enemySO;
    private AbilityScriptableObject _abilitySO;
    // Referencias a outros scripts
    private Enemy _enemy;
    // Controlam o state do ator
    private enum AbilityState { ready, active, cooldown }
    private enum EnemyState { wandering, chase, attack, flee }
    private AbilityState _abilityState = AbilityState.cooldown;
    [SerializeField] private EnemyState _enemyState;
    private float _stateTime;
    private float _abilityStateTime;
    private float _abilityCooldownTime;
    private Transform _target = null;

    private void Awake() {
        _enemy = GetComponent<Enemy>();
        _abilitySO = _enemySO.enemyAbility;
    }

    private void OnEnable() {
        _enemy.SetActualHealth(_enemySO.maxHealth);
        _stateTime = Random.Range(_enemySO.minWanderingTime, _enemySO.maxWanderingTime);
        _enemyState = EnemyState.wandering;
    }

    //todo Organizar a lógica dos states
    private void FixedUpdate() {
        if (_enemy.GetActualHealth() < 0) Destroy(this.gameObject);
        // Se recebeu um tiro nesse frame, armazena de quem veio o tiro
        
        // Olha para o lado em que está se movendo
        if (_enemy.ai.velocity.x > 0.1f) _spriteRenderer.flipX = false;
        else if (_enemy.ai.velocity.x < -0.1f) _spriteRenderer.flipX = true;

        switch (_enemyState){
            case EnemyState.wandering:
                // Velocidade padrão do state
                _enemy.ai.maxSpeed = _enemySO.wanderingSpeed;
                // Após chegar no destino, espera o tempo acabar 
                if (!_enemy.ai.pathPending && (_enemy.ai.reachedEndOfPath || !_enemy.ai.hasPath)) {
                    if (_stateTime > 0) {
                        _stateTime -= Time.fixedDeltaTime;
                    } else {
                        _enemy.ai.destination = _enemy.PickRandomPoint(_enemy.ai.position, _enemySO.searchRadius);
                        _enemy.ai.SearchPath();
                        _stateTime = Random.Range(_enemySO.minWanderingTime, _enemySO.maxWanderingTime);
                    }
                }
                // Condições para sair do state
                if (_enemy.GetLastAttacker() != null) { // Tomar tiro
                    // Se receber tiro, marca o player que atirou como target e muda de state
                    _target = _enemy.GetLastAttacker();
                    _stateTime = _enemySO.attackStateDuration;
                    _enemyState = EnemyState.flee;
                    //Debug.Log(gameObject.name + " is fleeing after wandering");
                    return;
                }
                // Procura um alvo
                _target = _enemySO.TargetInSight(gameObject, "Player");
                if (_target != null) { // Ver o player
                    _stateTime = _enemySO.chaseStateDuration;
                    _enemyState = EnemyState.chase;
                    //Debug.Log(gameObject.name + " is chasing after wandering");
                    return;
                }
                //print("tellaris wandering end");
            break;
            case EnemyState.chase:
                // Velocidade padrão do state
                _enemy.ai.maxSpeed = _enemySO.chaseStateSpeed;
                // Segue o alvo
                if (_target != null) {
                    _enemy.ai.destination = _target.position;
                    _enemy.ai.SearchPath();
                }

                // Condições para sair do state
                if (_enemy.GetLastAttacker() != null) { // Tomar tiro
                    // Se receber tiro, marca o player que atirou como target e muda de state
                    _target = _enemy.GetLastAttacker();
                    _enemyState = EnemyState.flee;
                    //Debug.Log(gameObject.name + " is fleeing after chasing");
                }
                if (_stateTime > 0) { // Duração do state acabar
                    _stateTime -= Time.fixedDeltaTime;
                } else {
                    _stateTime = _enemySO.attackStateDuration;
                    _enemyState = EnemyState.attack;
                    //Debug.Log(gameObject.name + " is attacking after chasing");
                    return;
                }
                //print("tellaris chase end");
            break;
            case EnemyState.attack:
                if (_stateTime > 0) { // Duração do state acabar
                    _stateTime -= Time.fixedDeltaTime;
                } else {
                    _target = null;
                    _enemyState = EnemyState.wandering;
                    //Debug.Log(gameObject.name + " is wandering after attacking");
                    return;
                }
                // Condições para sair do state
                if (_enemy.GetLastAttacker() != null) { // Tomar tiro
                    // Se receber tiro, marca o player que atirou como target e muda de state
                    _target = _enemy.GetLastAttacker();
                    _enemyState = EnemyState.flee;
                    //Debug.Log(gameObject.name + " is fleeing after attacking");
                }
                //print("tellaris  attack end");
            break;
            case EnemyState.flee:
                // Velocidade padrão do state
                _enemy.ai.maxSpeed = _enemySO.fleeStateSpeed;
                if (_enemy.GetLastAttacker()) { // Se ainda tem registrado o ultimo ataque remove o registro e cria um caminho pra andar
                    Vector3 direction = (_enemy.ai.position - _enemy.GetLastAttacker().position).normalized;
                    _enemy.ai.destination = _enemy.ai.position + direction * _enemySO.fleeDistance;
                    _enemy.ai.SearchPath();
                    _enemy.SetLastAttacker(null); 
                } 
                
                if (!_enemy.ai.pathPending && (_enemy.ai.reachedEndOfPath || !_enemy.ai.hasPath)) { // Se acabou o caminho, ataca
                    //Debug.Log(gameObject.name + " is attacking after fleeing");
                    _enemyState = EnemyState.attack;
                    return;
                }

                //print("tellaris flee end");
            break;
        }

        switch (_abilityState){
            case AbilityState.ready:
                if (_enemyState == EnemyState.attack) {
                    // Verifica em uma área maior se há visão do player, se sim, usa a habilidade mirando nele
                    if (_target) _abilitySO.Activate(gameObject, _target);
                    // Após usar a habilidade se move em certa direção
                    _enemy.ai.destination = _enemy.PickRandomPoint(_enemy.ai.position, _enemySO.searchRadius);
                    _enemy.ai.SearchPath();
                    _abilityState = AbilityState.active;
                    _abilityStateTime = _abilitySO.GetActiveTime();
                }
            break;
            case AbilityState.active:
                if (_abilityStateTime > 0) _abilityStateTime -= Time.fixedDeltaTime;
                else {
                    _abilityState = AbilityState.cooldown;
                    _abilityCooldownTime = _abilitySO.GetCooldownTime();
                }
            break;
            case AbilityState.cooldown:
                if (_abilityCooldownTime > 0) _abilityCooldownTime -= Time.fixedDeltaTime;
                else _abilityState = AbilityState.ready;
            break;
        }
    }
}
