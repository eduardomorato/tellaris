using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random=UnityEngine.Random;
using Pathfinding;
using System;


public class Creeper : MonoBehaviour
{
// Sprite, inimigo e abilidades do inimigo
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private EnemyScriptableObject _enemySO;
    [SerializeField] private float _distanceToExplode;
    private AbilityScriptableObject _abilitySO;
    // Referencias a outros scripts
    private Enemy _enemy;
    // Controlam o state do ator
    private enum EnemyState { chase, attack }
    [SerializeField] private EnemyState _enemyState;

    private void Awake() {
        _enemy = GetComponent<Enemy>();
        _abilitySO = _enemySO.enemyAbility;
    }

    private void OnEnable() {
        _enemy.SetActualHealth(_enemySO.maxHealth);
        _enemyState = EnemyState.chase;
        _enemy.ai.maxSpeed = _enemySO.chaseStateSpeed;
    }

    // SE O CREEPER TEM UM TARGET, CHASE, SE ESTIVER NO ALCANCE, ATTACK
    //todo Organizar a lógica dos states
    private void FixedUpdate() {
        if (_enemy.GetActualHealth() < 0) Destroy(this.gameObject);
        // Olha para o lado em que está se movendo
        if (_enemy.ai.velocity.x > 0.1f) _spriteRenderer.flipX = false;
        else if (_enemy.ai.velocity.x < -0.1f) _spriteRenderer.flipX = true;

        switch (_enemyState){
            case EnemyState.chase:
                if (_enemy.GetActualTarget()) { 
                    _enemy.ai.destination = _enemy.GetActualTarget().position; 
                    _enemy.ai.SearchPath();
                    float distance = Vector3.Distance (transform.position, _enemy.GetActualTarget().position);

                    if (distance < _distanceToExplode) {
                        _enemyState = EnemyState.attack;
                    } 
                } else if (_enemy.GetActualTarget() == null) _enemyState = EnemyState.attack;;
            break;
            case EnemyState.attack:
                    Debug.Log(gameObject.name + " is attacking");
                    _abilitySO.Activate(gameObject, _enemy.GetActualTarget());
            break;
        }
    }
}
