using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random=UnityEngine.Random;
using Pathfinding;
using System;

// Funções genéricas para inimigos

public class Enemy : MonoBehaviour
{
    public IAstarAI ai {get; private set; }
    [SerializeField] private float _actualHealth;
    [SerializeField] private Transform _lastAttackedBy;
    private Transform _target;

    private void Awake() {
        ai = GetComponent<IAstarAI>();
    }

    IEnumerator Knockback (IAstarAI ai, Vector3 dir, float duration) {
    ai.isStopped = true;
    for (float t = 0; t < duration; t += Time.deltaTime) {
        ai.Move(dir * Time.deltaTime * 1 / (t + 0.1f));
        yield return null;
    }
    ai.isStopped = false;
    }

    public void TakeDamage(Transform obj, float damage){
        print(this.gameObject.name + " took " + damage + " damage from " + obj);
        _lastAttackedBy = obj;
        _actualHealth -= damage;
        Vector3 direction = (ai.position - _lastAttackedBy.position).normalized;
        StartCoroutine(Knockback(ai, direction, 0.02f));
    }

    public Vector3 PickRandomPoint (Vector3 from, float radius) {
        Vector3 point = Random.insideUnitCircle * radius;
        point.z = 0;
        point += from;
        return point;
    }

    public float GetActualHealth(){
        return _actualHealth;
    }

    public void SetActualHealth(float health){
        _actualHealth = health;
    }

    public void SetActualTarget(Transform target){
        _target = target;
    }

    public Transform GetActualTarget(){
        return _target;
    }

    public Transform GetLastAttacker(){
        return _lastAttackedBy;
    }

    public void SetLastAttacker(Transform lastAtkr){
        _lastAttackedBy = lastAtkr;
    }
}
