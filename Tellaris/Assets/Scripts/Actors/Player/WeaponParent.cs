using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponParent : MonoBehaviour
{
    private Vector2 _pointerPosition;
    private SpriteRenderer _characterRenderer, _weaponRenderer;
    //[SerializeField] private Animator _animator;
    //private bool _isAttacking = false;
    private PlayerController _playerController;
    private Player _player;

    private void Start() {
        _playerController = GetComponentInParent<PlayerController>();
        _player = GetComponentInParent<Player>();
        _weaponRenderer = GetComponentInChildren<SpriteRenderer>();
        _characterRenderer = _player.GetSpriteRenderer();
    }

    // Update is called once per frame
    void Update(){
            Vector2 direction  = (_pointerPosition - (Vector2)transform.position).normalized;
            transform.right = direction;

            Vector2 scale = transform.localScale;
            if (direction.x < -0.2) {
                scale.y = -1;
                _characterRenderer.flipX = true;
            } else if (direction.x > 0.2) {
                scale.y = 1;
                _characterRenderer.flipX = false;
            }
            transform.localScale = scale;

            if (transform.eulerAngles.z > 0 && transform.eulerAngles.z < 180)
                _weaponRenderer.sortingOrder = _characterRenderer.sortingOrder - 1;
            else
                _weaponRenderer.sortingOrder = _characterRenderer.sortingOrder + 1;
    }

    // Recebe a posição do mouse vinda do player controller
    public void SetPointerPosition(Vector2 index){
        _pointerPosition = index;
    }
}
