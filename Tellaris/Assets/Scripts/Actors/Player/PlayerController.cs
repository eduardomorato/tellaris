using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Pathfinding;

public class PlayerController : MonoBehaviour
{
    //private Vector2 _pointerInput;
    [SerializeField] private InputActionReference _pointerPosition, _leftClick, _rightClick, _movement, _jump;
    [SerializeField] private WeaponParent _weapon;
    [SerializeField] private Transform _bulletPos;
    [SerializeField] private LayerMask _obstacleLayer;
    [SerializeField] private float _jumpWaitTime;
    //BoxCollider2D _boxCol;
    CapsuleCollider2D _capsCol;
    private float _jumpWTimer;
    private Rigidbody2D _rb;
    private Player _player;
    private AbilityScriptableObject _playerAbilitySO;

    private enum AbilityState { ready, active, cooldown }
    private AbilityState _abilityState = AbilityState.cooldown;
    private float _abilityStateTime;
    private float _abilityCooldownTime;

    public Vector3 GetPointerInput(){ // Returns the pointer position in the world
        Vector3 mousePos = _pointerPosition.action.ReadValue<Vector2>();
        mousePos.z = Camera.main.nearClipPlane;
        return Camera.main.ScreenToWorldPoint(mousePos);
    }

    private void Awake() {
        _rb = GetComponent<Rigidbody2D>();
        _player = GetComponent<Player>();
        //_boxCol = GetComponent<BoxCollider2D>();
        _capsCol = GetComponent<CapsuleCollider2D>();
        _playerAbilitySO = _player.GetAbilitySO();
    }

    private void FixedUpdate () {
		_weapon.SetPointerPosition(GetPointerInput());
        float speed = _player.GetActualMoveSpeed();

        float moveInput = _movement.action.ReadValue<Vector2>().x;

        //todo Adicionar valores ao player
        float walkAcceleration = 150;
        float airAcceleration = 50;
        float groundDeceleration = 80;

        // Diferentes valores para quando o jogador está no ar ou no chão
        bool grounded = isGrounded();
        float acceleration = grounded ? walkAcceleration : airAcceleration;
        float deceleration = grounded ? groundDeceleration : 0;
        // Altera a velocidade e depois a aplica
        Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
        if (moveInput != 0) velocity.x = Mathf.MoveTowards(velocity.x, speed * moveInput, acceleration * Time.fixedDeltaTime);
        else velocity.x = Mathf.MoveTowards(velocity.x, 0, deceleration * Time.deltaTime);
        GetComponent<Rigidbody2D>().velocity = new Vector2(velocity.x, GetComponent<Rigidbody2D>().velocity.y);
        // Pula se estiver no chão ou se o tempo de espera após sair do chão não estiver acabado
        if (_jump.action.IsPressed()) if (grounded || _jumpWTimer > 0) Jump();

        switch (_abilityState){
            case AbilityState.ready:
                if (_leftClick.action.IsPressed()) {
                    // Verifica em uma área maior se há visão do player, se sim, usa a habilidade mirando nele
                    _playerAbilitySO.Activate(gameObject, _bulletPos.transform);
                    // Após usar a habilidade se move em certa direção
                    _abilityState = AbilityState.active;
                    _abilityStateTime = _playerAbilitySO.GetActiveTime();
                }
            break;
            case AbilityState.active:
                if (_abilityStateTime > 0) _abilityStateTime -= Time.fixedDeltaTime;
                else {
                    _abilityState = AbilityState.cooldown;
                    _abilityCooldownTime = _playerAbilitySO.GetCooldownTime();
                }
            break;
            case AbilityState.cooldown:
                if (_abilityCooldownTime > 0) _abilityCooldownTime -= Time.fixedDeltaTime;
                else _abilityState = AbilityState.ready;
            break;
        }

        // Tempo de espera após não poder pular é reduzido ou resetado
        if (grounded) _jumpWTimer = _jumpWaitTime;
        else if (_jumpWTimer > 0) _jumpWTimer -= Time.fixedDeltaTime;

	}

    private void Jump(){
        _rb.velocity = new Vector2(_rb.velocity.x, _player.GetJumpSpeed() * Time.fixedDeltaTime);
    }

    private bool isGrounded(){
        float extra = 1f;
        RaycastHit2D hit =  Physics2D.BoxCast(_capsCol.bounds.center, _capsCol.bounds.size * 0.95f, 0f, Vector2.down, extra, _obstacleLayer);
        return (hit.collider != null);
    }
}
