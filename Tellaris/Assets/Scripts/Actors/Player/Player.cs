using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random=UnityEngine.Random;
using Pathfinding;
using System;

public class Player : MonoBehaviour
{
    [SerializeField] private float _maxHealth = 200;
    [SerializeField] private float _actualHealth = 200;
    [SerializeField] private float _moveSpeed = 10;
    [SerializeField] private float _jumpSpeed = 10;
    [SerializeField] private SpriteRenderer _playerSprite;
    private bool _alive = true;

    [SerializeField] private UserInterfaceDataScriptableObject _uiData;
    [SerializeField] private AbilityScriptableObject _abilitySO;

    private Transform _lastAttackedBy;
    private Transform _target;

    public void TakeDamage(Transform gameobject, float damage){
        _lastAttackedBy = gameobject;
        _actualHealth -= damage;
        Vector3 direction = (this.gameObject.transform.position - _lastAttackedBy.position).normalized;
        Knockback(gameobject);
    }

    public void Knockback(Transform sender, float force = 500f){
        Vector3 direction = (this.gameObject.transform.position - sender.position).normalized;
        gameObject.GetComponent<Rigidbody2D>().AddForce(direction * force);
    }

    private void Update() {
        if (_actualHealth < 0) {
            _actualHealth = 0;
            _alive = false;
        }
        _uiData.SetPlayerMaxHealth(_maxHealth);
        _uiData.SetPlayerHealth(_actualHealth);
        _uiData.SetPlayerAlive(_alive);
        if (_alive == false) Destroy(this.gameObject);
    }


    public AbilityScriptableObject GetAbilitySO(){
        return _abilitySO;
    }

    public float GetActualHealth(){
        return _actualHealth;
    }

    public float GetActualMoveSpeed(){
        return _moveSpeed;
    }

    public float GetJumpSpeed(){
        return _jumpSpeed;
    }

    public SpriteRenderer GetSpriteRenderer(){
        return _playerSprite;
    }

    public void SetActualHealth(float health){
        _actualHealth = health;
    }

    public void SetActualTarget(Transform target){
        _target = target;
    }

    public Transform GetLastAttacker(){
        return _lastAttackedBy;
    }

    public void SetLastAttacker(Transform lastAtkr){
        _lastAttackedBy = lastAtkr;
    }
}
